package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringAuthenticationProvider implements AuthenticationProvider {

    @Qualifier("myUserDetail")
    @Autowired
    private UserDetailsService userDetailsService;

//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication, "非验证类型");
//        //账号
//        String username = authentication.getName();
//        //密码
//        String password = authentication.getCredentials().toString();
//        List<SimpleGrantedAuthority> roles = new ArrayList<>();
//        if ("admin".equalsIgnoreCase(username)) {
//            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
//        } else if ("data".equalsIgnoreCase(username)) {
//            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
//            roles.add(new SimpleGrantedAuthority("ROLE_DBA"));
//        } else {
//            throw new UsernameNotFoundException("用户名/密码无效");
//        }
//        return new UsernamePasswordAuthenticationToken(username, password, roles);
//    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication, "非验证类型");

        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        List<SimpleGrantedAuthority> roles = new ArrayList<>();

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (userDetails == null) {
            throw new UsernameNotFoundException("用户名/密码无效");
        }


//        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails, password, roles);

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password, roles);
        token.setDetails(userDetails);

        return token;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }


}
