package com.example.demo;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;


@Component
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                //"/resources/**", "/signup", "/about" 路径为免验证访问
                .antMatchers("/resources/**", "/signup", "/about","/login","/logout")
                .permitAll()

                //"/admin/**" 路径为 ADMIN 角色可访问
                .antMatchers("/admin/**")
                .hasRole("ADMIN")
                // "/db/**" 路径为 ADMIN 和 DBA 角色同时拥有时可访问
                .antMatchers("/db/**")
                .access("hasRole('ADMIN') and hasRole('DBA')")

                //未匹配路径为登陆可访问
                .anyRequest().authenticated()
                .and()
                .formLogin()
                //设置登陆页面
                .loginPage("/login")
                //允许所有人进行访问此路径
                .permitAll();

        //关闭csrf保护
//                    .and().csrf().disable();

    }
}
