package com.antball.mybatis.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_activity_increase_rate")
public class TActivityIncreaseRate {
    /**
     * 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 类型:1全场加息 2首投加息 3尾投加息
     */
    private Integer type;

    /**
     * 是否激活 
     */
    @Column(name = "is_activate")
    private String isActivate;

    /**
     * 加息名称
     */
    private String name;

    /**
     * 简介
     */
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取类型:1全场加息 2首投加息 3尾投加息
     *
     * @return type - 类型:1全场加息 2首投加息 3尾投加息
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置类型:1全场加息 2首投加息 3尾投加息
     *
     * @param type 类型:1全场加息 2首投加息 3尾投加息
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取是否激活 
     *
     * @return is_activate - 是否激活 
     */
    public String getIsActivate() {
        return isActivate;
    }

    /**
     * 设置是否激活 
     *
     * @param isActivate 是否激活 
     */
    public void setIsActivate(String isActivate) {
        this.isActivate = isActivate;
    }

    /**
     * 获取加息名称
     *
     * @return name - 加息名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置加息名称
     *
     * @param name 加息名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取简介
     *
     * @return remark - 简介
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置简介
     *
     * @param remark 简介
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}