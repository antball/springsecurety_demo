package com.antball.mybatis.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_activity_increase_rate_detail")
public class TActivityIncreaseRateDetail {
    /**
     * 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 活动编号
     */
    @Column(name = "activity_id")
    private Integer activityId;

    /**
     * 加息利率
     */
    private BigDecimal rate;

    /**
     * 状态0待审核 1初审通过 2终审通过
     */
    private Integer state;

    /**
     * 活动开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 活动结束时间
     */
    @Column(name = "stop_time")
    private Date stopTime;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取活动编号
     *
     * @return activity_id - 活动编号
     */
    public Integer getActivityId() {
        return activityId;
    }

    /**
     * 设置活动编号
     *
     * @param activityId 活动编号
     */
    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    /**
     * 获取加息利率
     *
     * @return rate - 加息利率
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 设置加息利率
     *
     * @param rate 加息利率
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * 获取状态0待审核 1初审通过 2终审通过
     *
     * @return state - 状态0待审核 1初审通过 2终审通过
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态0待审核 1初审通过 2终审通过
     *
     * @param state 状态0待审核 1初审通过 2终审通过
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取活动开始时间
     *
     * @return start_time - 活动开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置活动开始时间
     *
     * @param startTime 活动开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取活动结束时间
     *
     * @return stop_time - 活动结束时间
     */
    public Date getStopTime() {
        return stopTime;
    }

    /**
     * 设置活动结束时间
     *
     * @param stopTime 活动结束时间
     */
    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }
}