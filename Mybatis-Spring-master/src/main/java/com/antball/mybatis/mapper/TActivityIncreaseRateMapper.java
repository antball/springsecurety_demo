package com.antball.mybatis.mapper;

import com.antball.mybatis.model.TActivityIncreaseRate;
import tk.mybatis.mapper.common.Mapper;

public interface TActivityIncreaseRateMapper extends Mapper<TActivityIncreaseRate> {
}