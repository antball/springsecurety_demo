package com.antball.mybatis.mapper;

import com.antball.mybatis.model.TActivityIncreaseRateDetail;
import tk.mybatis.mapper.common.Mapper;

public interface TActivityIncreaseRateDetailMapper extends Mapper<TActivityIncreaseRateDetail> {
}