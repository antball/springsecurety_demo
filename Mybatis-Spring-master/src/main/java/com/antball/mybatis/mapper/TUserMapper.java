package com.antball.mybatis.mapper;

import com.antball.mybatis.model.TUser;
import tk.mybatis.mapper.common.Mapper;

public interface TUserMapper extends Mapper<TUser> {
}