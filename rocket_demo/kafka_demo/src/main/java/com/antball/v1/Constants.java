package com.antball.v1;

/**
 * @Auther: huangsj
 * @Date: 2018/8/23 09:31
 * @Description:
 */
public interface Constants {

    String KAFKA_PRODUCER_TOPIC = "producer.topic";
}
