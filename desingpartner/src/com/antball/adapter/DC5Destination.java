package com.antball.adapter;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:04
 * @Description:
 */
public interface DC5Destination {

    int output5V();
}
