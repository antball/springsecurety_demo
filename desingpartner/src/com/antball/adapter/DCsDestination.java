package com.antball.adapter;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:27
 * @Description:
 */
public interface DCsDestination {

    int output5V();
    int output9V();
    int output12V();
    int output24V();
}
