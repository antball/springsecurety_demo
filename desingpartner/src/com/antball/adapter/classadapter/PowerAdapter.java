package com.antball.adapter.classadapter;

import com.antball.adapter.AC220Source;
import com.antball.adapter.DC5Destination;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:06
 * @Description:
 */


//java单继承的缘故，Destination类必须是接口，以便于Adapter去继承Source并实现Destination
//public class Adapter 适配器 extends Adaptee 适配者 implements Target  抽象目标
public class PowerAdapter extends AC220Source implements DC5Destination {


    @Override
    public int output5V() {
        int output = output220V();
        return output / 44;
    }
}
