package com.antball.adapter.objectadapter;

import com.antball.adapter.AC220Source;
import com.antball.adapter.DC5Destination;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:13
 * @Description:
 */
public class PowerAdapter implements DC5Destination {


    protected AC220Source source;

    public PowerAdapter(AC220Source source){
        this.source = source;
    }

    @Override
    public int output5V() {
        int output = 0;
        if(source!=null) {
            output = source.output220V() / 44;
        }
        return  output;
    }
}
