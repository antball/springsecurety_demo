package com.antball.adapter.interfaceadapter;

import com.antball.adapter.AC220Source;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:33
 * @Description:
 */
public class Power5VAdapter extends PowerAdapter {


    public Power5VAdapter(AC220Source source) {
        super(source);
    }

    @Override
    public int output5V() {
        int output = 0;
        if (source != null) {
            output = source.output220V() / 44;
        }
        return output;
    }
}
