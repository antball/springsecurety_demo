package com.antball.adapter.interfaceadapter;

import com.antball.adapter.AC220Source;
import com.antball.adapter.DCsDestination;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:28
 * @Description:
 */
public abstract class PowerAdapter implements DCsDestination {


    protected AC220Source source;

    public PowerAdapter(AC220Source source){
        this.source = source;
    }

    @Override
    public int output5V() {
        return 0;
    }

    @Override
    public int output9V() {
        return 0;
    }

    @Override
    public int output12V() {
        return 0;
    }

    @Override
    public int output24V() {
        return 0;
    }
}
