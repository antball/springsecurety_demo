package com.antball.adapter;


import com.antball.adapter.interfaceadapter.Power5VAdapter;
import com.antball.adapter.interfaceadapter.PowerAdapter;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 10:09
 * @Description:
 */
public class Context {



    public static void main(String[] args) {
        m();
    }

    public static void m() {

//        类适配器使用  暴露了Source类的方法，一般不用
        DC5Destination dc5Destination = new com.antball.adapter.classadapter.PowerAdapter();
        System.out.println( dc5Destination.output5V());



        AC220Source ac220Source =  new AC220Source();

//        类适配器使用
        dc5Destination = new com.antball.adapter.objectadapter.PowerAdapter(ac220Source);
        System.out.println(dc5Destination.output5V());


//        接口适配器
        DCsDestination dcs = new Power5VAdapter(ac220Source);
        System.out.println(dcs.output5V());



        //匿名类
        dcs = new PowerAdapter(ac220Source){
            @Override
            public int output5V() {
                int output = 0;
                if (this.source != null) {
                    output = this.source.output220V() / 44;
                }
                return output;
            }

            @Override
            public int output24V() {
                int output = 0;
                if (this.source != null) {
                    output = this.source.output220V() / 10;
                }
                return output;
            }
        };

        System.out.println(dcs.output5V());
        System.out.println(dcs.output24V());



    }




}
