package com.antball.strategy;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 15:32
 * @Description:
 */
public interface IStrategyCash {

    Double getResult(Double money);

    void setRebate(Double rebate);

    void setConditionAndReturn(Double condition, Double returnMoney);
}
