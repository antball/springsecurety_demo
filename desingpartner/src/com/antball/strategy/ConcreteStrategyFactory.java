package com.antball.strategy;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 17:19
 * @Description:
 */
public class ConcreteStrategyFactory {

//    private static final String[] CUSH_CLASS_NAME = {
//            "main.java.celue.celue5.CushNormal",
//            "main.java.celue.celue5.CushRebate",
//            "main.java.celue.celue5.CushReturn"
//    };

    public static  <T extends IStrategyCash> T getTheCash(Class<T> c,Class[] paramsType, Object[] parmas) {

        try {
            Class<?> clazz=Class.forName(c.getName());

//            int type2=Integer.parseInt(type);
//            Class<?> clazz=Class.forName(CUSH_CLASS_NAME[type2]);

            Constructor<?> constructor=clazz.getConstructor(paramsType);


            return (T)constructor.newInstance(parmas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }



//    public static  <T extends IStrategyCash> T getTheCash(Class<T> c){
//        IStrategyCash cash = null;
//        try {
//            cash = (IStrategyCash) Class.forName(c.getName()).newInstance();
//
//
//
////            Class stu = Class.forName("ready.nextyear.Student");
////            Constructor constructor = stu.getConstructor(int.class, String.class);
////            Student student = (Student) constructor.newInstance(23, "lily");
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return (T) cash;
//    }




    /*properties文件;


    ES=EarthShaker
    SF=NeverMore
    TF=Pudge

    public static Properties init() {
        // 从properties文件中读取shortName对应的完整包名
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/nameMapping.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static Hero choose(String shortName) {
        Hero hero = null;
        try {
            String fullName = HeroesFactory.init().getProperty(shortName);
            hero = (Hero) Class.forName(fullName).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hero;
    }


    public class BoardNoContext {

    private Map<String, BoardNoService> mapStractegy = new HashMap<String, BoardNoService>();

    public Map<String, BoardNoService> getMapStractegy() {
        return mapStractegy;
    }
    public void setMapStractegy(Map<String, BoardNoService> mapStractegy) {
        this.mapStractegy = mapStractegy;
    }
    public String getNewBoardNo(String channelCode){
        return this.mapStractegy.get(channelCode).produceBoardNo(channelCode);
    }
}


    */

}
