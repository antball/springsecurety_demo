package com.antball.strategy;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 13:59
 * @Description:
 */
public class Context {

    public static void main(String[] args) {
        m();
        System.out.println(StrategyEnum.Normal.ordinal()+"  "+StrategyEnum.Normal.name());
        System.out.println();
    }


    /**
     * https://blog.csdn.net/zhongxuebin_xq/article/details/81275958
     *
     */
    public static void m() {

        Double money =200d;
        Double rate = 0.8d;
        Double condition = 100d;
        Double returnMoney = 30d;


        IStrategyCash cash = ConcreteStrategyFactory.getTheCash(StrategyCashNormal.class,null,null);
        System.out.println( cash.getResult(money));

//        Class[] paramTypes2 = {String.class};
//        Object[] params2 = {"0.8"};
        cash = ConcreteStrategyFactory.getTheCash(StrategyCashRebate.class,new Class[]{Double.class},new Object[]{rate});
        System.out.println( cash.getResult(money));


        cash = ConcreteStrategyFactory.getTheCash(StrategyCashReturn.class,new Class[]{Double.class,Double.class},new Object[]{condition,returnMoney});
        System.out.println( cash.getResult(money));



        System.out.println("--------------------");



                cash = ConcreteStrategyFactory2.getTheCash(StrategyCashNormal.class);
        System.out.println( cash.getResult(money));

//        Class[] paramTypes2 = {String.class};
//        Object[] params2 = {"0.8"};
        cash = ConcreteStrategyFactory2.getTheCash(StrategyCashRebate.class);
        cash.setRebate(rate);
        System.out.println( cash.getResult(money));

        cash = ConcreteStrategyFactory2.getTheCash(StrategyCashReturn.class);
        cash.setConditionAndReturn(condition,returnMoney);
        System.out.println( cash.getResult(money));



        System.out.println("--------------------");



        System.out.println(caculateTotalPrice(StrategyCashNormal.class,money,rate,condition,returnMoney));
        System.out.println(caculateTotalPrice(StrategyCashRebate.class,money,rate,condition,returnMoney));
        System.out.println(caculateTotalPrice(StrategyCashReturn.class,money,rate,condition,returnMoney));



        System.out.println("--------------------");



        System.out.println(caculateTotalPriceByName(StrategyEnum.Normal.name().toLowerCase(),money,rate,condition,returnMoney));
        System.out.println(caculateTotalPriceByName(StrategyEnum.Rebate.name().toLowerCase(),money,rate,condition,returnMoney));
        System.out.println(caculateTotalPriceByName(StrategyEnum.Return.name().toLowerCase(),money,rate,condition,returnMoney));


    }

    public  static Double caculateTotalPrice(Class<? extends IStrategyCash> c,Double money, Double rate,Double condition,Double returnMoney){
        IStrategyCash cash = ConcreteStrategyFactory2.getTheCash(c);
        cash.setRebate(rate);
        cash.setConditionAndReturn(condition,returnMoney);

        return cash.getResult(money);
    }




    public  static Double caculateTotalPriceByName(String shortName,Double money, Double rate,Double condition,Double returnMoney){
        IStrategyCash cash = ConcreteStrategyFactory3.getTheCash(shortName);
        cash.setRebate(rate);
        cash.setConditionAndReturn(condition,returnMoney);

        return cash.getResult(money);
    }
}
