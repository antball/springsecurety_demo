package com.antball.strategy;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 15:38
 * @Description:
 */
public class StrategyCashReturn implements IStrategyCash {


    private Double returnMoney=0.0;
    private Double condition=0.0;

    public StrategyCashReturn() {
    }

    public StrategyCashReturn(Double condition, Double returnMoney){
        this.condition=condition;
        this.returnMoney=returnMoney;
    }


    @Override
    public void setRebate(Double rebate) {

    }

    @Override
    public void setConditionAndReturn(Double condition, Double returnMoney){
        this.condition=condition;
        this.returnMoney=returnMoney;
    }


    /**
     * 每满condition送returnMoney
     * @param money
     * @return
     */
    @Override
    public Double getResult(Double money) {
        Double result=money;

        if (money>=condition) {
            result=money-Math.floor(money/condition)*returnMoney;
        }

        return result;
    }

}
