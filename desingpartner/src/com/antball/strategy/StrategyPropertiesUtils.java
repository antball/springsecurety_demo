package com.antball.strategy;

import java.io.*;
import java.util.*;

/**
 * @Auther: huangsj
 * @Date: 2018/11/7 13:22
 * @Description:
 */
public class StrategyPropertiesUtils {

    public static final Properties p = new Properties();
    public static final String path = ".\\src\\com\\antball\\strategy\\strategy.properties";

    /**
     * 通过类装载器 初始化Properties
     */
    public static Properties init() {
        //转换成流
        InputStream inputStream = null;//= StrategyPropertiesUtils.class.getClassLoader().getResourceAsStream("file.properties");


        File file = new File(path);

        if (file.exists()) {
            try {
                inputStream = new FileInputStream(path);
                //从输入流中读取属性列表（键和元素对）
                p.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


//            LinkedHashMap<String,String> pros = new LinkedHashMap<>();
//            pros.put("normal","ffffff");
//            pros.put("aaaaa","11111");
//            update(pros,"update test");

        } else {

            LinkedHashMap<String,String> pros = new LinkedHashMap<>();
            pros.put("normal","com.antball.strategy.StrategyCashNormal");
            pros.put("rebate","com.antball.strategy.StrategyCashRebate");
            pros.put("return","com.antball.strategy.StrategyCashReturn");
            update(pros,"");
        }


        return p;
    }

    /**
     * 通过key获取value
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        return p.getProperty(key);
    }

    /**
     * 修改或者新增key
     *
     * @param key
     * @param value
     */
    public static void update(LinkedHashMap<String,String> propertys,String comments) {

        for(Map.Entry<String, String> entry: propertys.entrySet()){
            p.setProperty(entry.getKey(), entry.getValue());
        }


        FileOutputStream oFile = null;
        try {
            oFile = new FileOutputStream(path);
            //将Properties中的属性列表（键和元素对）写入输出流
            p.store(oFile, comments);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                oFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 通过key删除value
     *
     * @param key
     */
    public static void delete(String key) {
        p.remove(key);
        FileOutputStream oFile = null;
        try {
            oFile = new FileOutputStream(path);
            p.store(oFile, "");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                oFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 循环所有key value
     */
    public static void list() {
        Enumeration en = p.propertyNames(); //得到配置文件的名字
        while (en.hasMoreElements()) {
            String strKey = (String) en.nextElement();
            String strValue = p.getProperty(strKey);
            System.out.println(strKey + "=" + strValue);
        }
    }
}
