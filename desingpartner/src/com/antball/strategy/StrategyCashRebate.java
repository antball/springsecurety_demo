package com.antball.strategy;

import java.math.BigDecimal;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 15:38
 * @Description:
 */
public class StrategyCashRebate implements IStrategyCash {

    private Double rebate=1.0;

    public StrategyCashRebate() {
    }

    public StrategyCashRebate(Double rebate){
        this.rebate=rebate;
    }

    @Override
    public void setConditionAndReturn(Double condition, Double returnMoney) {

    }

    @Override
    public void setRebate(Double rebate) {
        this.rebate = rebate;
    }

    @Override
    public Double getResult(Double money) {
        return money*rebate;
    }
}
