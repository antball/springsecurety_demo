package com.antball.strategy;

/**
 * @Auther: huangsj
 * @Date: 2018/11/3 15:38
 * @Description:
 */
public class StrategyCashNormal implements IStrategyCash {

    @Override
    public void setRebate(Double rebate) {

    }

    @Override
    public void setConditionAndReturn(Double condition, Double returnMoney) {

    }

    @Override
    public Double getResult(Double money) {
        return money;
    }

}
