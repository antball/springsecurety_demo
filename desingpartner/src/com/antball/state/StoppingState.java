package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:07
 * @Description:
 */
public class StoppingState extends LiftState {
    @Override
    public void open() {
        context.setState(Context.openningState);
        context.open();
    }

    @Override
    public void close() {

    }

    @Override
    public void run() {
        context.setState(Context.runningState);
        context.run();
    }

    @Override
    public void stop() {
        System.out.println("lift stop ...");
    }
}
