package com.antball.state;

import com.antball.strategy.ConcreteStrategyFactory;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:09
 * @Description:
 */
public class Context {

    public  static OpenningState openningState = new OpenningState();
    public static CloseingState closeingState =new CloseingState();
    public static RunningState runningState=new RunningState();
    public static StoppingState stoppingState=new StoppingState();


    private LiftState state;

    public Context(){
        openningState.setContext(this);
        closeingState.setContext(this);
        runningState.setContext(this);
        stoppingState.setContext(this);
    }

    public LiftState getState() {
        return state;
    }

    public void setState(LiftState state) {
        this.state = state;
    }


    public void open(){
        this.state.open();
    }

    public void close(){
        this.state.close();
    }

    public void run(){
        this.state.run();
    }

    public void stop(){
        this.state.stop();
    }
}
