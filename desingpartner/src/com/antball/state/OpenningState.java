package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:06
 * @Description:
 */
public class OpenningState extends LiftState {
    @Override
    public void open() {
        System.out.println("lift open ...");
    }

    @Override
    public void close() {
        context.setState(Context.closeingState);

        context.close();
    }

    @Override
    public void run() {

    }

    @Override
    public void stop() {

    }
}
