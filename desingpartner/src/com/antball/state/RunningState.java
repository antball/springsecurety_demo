package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:06
 * @Description:
 */
public class RunningState extends LiftState {
    @Override
    public void open() {
        System.out.println("lift running can not open");
    }

    @Override
    public void close() {
        System.out.println("lift running can not close");
    }

    @Override
    public void run() {
        System.out.println("lift run ...");
    }

    @Override
    public void stop() {
        context.setState(Context.stoppingState);
        context.stop();
    }
}
