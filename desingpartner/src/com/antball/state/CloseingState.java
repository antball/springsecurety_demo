package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:06
 * @Description:
 */
public class CloseingState extends LiftState {
    @Override
    public void open() {
        context.setState(Context.openningState);
        context.open();
    }

    @Override
    public void close() {

        System.out.println("lift close ...");

    }

    @Override
    public void run() {
        context.setState(Context.runningState);
        context.run();
    }

    @Override
    public void stop() {
        context.setState(Context.stoppingState);
        context.stop();
    }
}
