package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:06
 * @Description:
 */
public abstract class LiftState {

    protected Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public void open(){}

    public void close(){}

    public void run(){}

    public void stop(){}

}
