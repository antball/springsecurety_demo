package com.antball.state;

/**
 * @Auther: huangsj
 * @Date: 2018/11/8 17:36
 * @Description:
 */
public class Client {

    public static void main(String[] args) {
        m();
    }


    public static void m(){
        Context context=new Context();

        context.setState(Context.stoppingState);
        context.open();

        if(context.getState() instanceof OpenningState){
            context.close();
        }
        context.run();

        if(context.getState() instanceof CloseingState){
            context.open();
        }
        context.stop();
    }
}
