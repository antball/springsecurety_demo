package com.antball.decorator.concretecomponent;

import com.antball.decorator.DrinkConponent;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 16:24
 * @Description:
 */
public class DanCoffee extends DrinkConponent {
    @Override
    public String getDescription() {
        return "淡咖啡";
    }

    @Override
    public double cost() {
        return 80d;
    }
}
