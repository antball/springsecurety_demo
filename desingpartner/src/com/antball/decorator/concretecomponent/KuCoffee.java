package com.antball.decorator.concretecomponent;

import com.antball.decorator.DrinkConponent;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 16:23
 * @Description:
 */
public class KuCoffee extends DrinkConponent {

    @Override
    public String getDescription() {
        return "苦咖啡";
    }

    @Override
    public double cost() {
        return 100d;
    }
}
