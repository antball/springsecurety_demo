package com.antball.decorator;

import com.antball.decorator.concretecomponent.DanCoffee;
import com.antball.decorator.concretecomponent.KuCoffee;
import com.antball.decorator.concretedecorator.Icecreame;
import com.antball.decorator.concretedecorator.Milk;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 17:08
 * @Description:
 */
public class Context {

    public static void main(String[] args) {
        m();
    }

    public static void m() {
        /**
         * 关键在于抽像的装饰类，它里面关联抽像的构件类，
         */
        DrinkConponent /*抽像构件*/ conponent = new DanCoffee()/*具体构件*/;
        conponent = new Milk/*具体装饰类*/(conponent);
        conponent = new Icecreame(conponent);
        System.out.println(conponent.getDescription() + " " + conponent.cost());


        conponent = new KuCoffee();
        conponent = new Icecreame(conponent);
        System.out.println(conponent.getDescription() + " " + conponent.cost());

    }


}
