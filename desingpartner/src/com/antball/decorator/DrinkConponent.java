package com.antball.decorator;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 11:44
 * @Description:
 */
public abstract class DrinkConponent {

    String description = "Unknown Beverage";

    public abstract String getDescription();

    public abstract double cost();


}
