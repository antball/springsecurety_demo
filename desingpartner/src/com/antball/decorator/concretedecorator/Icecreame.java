package com.antball.decorator.concretedecorator;

import com.antball.decorator.CoffeeDecorator;
import com.antball.decorator.DrinkConponent;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 16:32
 * @Description:
 */
public class Icecreame extends CoffeeDecorator {

    public Icecreame(DrinkConponent conponent) {
        super.drinkConponent=conponent;
    }

    @Override
    public String getDescription() {
        return drinkConponent.getDescription()+"加冰淇淋";
    }

    @Override
    public double cost() {
        return drinkConponent.cost()+50;
    }
}
