package com.antball.decorator.concretedecorator;

import com.antball.decorator.CoffeeDecorator;
import com.antball.decorator.DrinkConponent;

/**
 * @Auther: huangsj
 * @Date: 2018/11/1 16:26
 * @Description:
 */
public class Milk extends CoffeeDecorator {


    public Milk(DrinkConponent drinkConponent){
            super.drinkConponent=drinkConponent;
    }

    @Override
    public String getDescription() {
        return drinkConponent.getDescription()+"加奶";
    }

    @Override
    public double cost() {
        return drinkConponent.cost()+30;
    }
}
