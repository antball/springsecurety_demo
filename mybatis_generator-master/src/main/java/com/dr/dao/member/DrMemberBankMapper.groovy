package com.dr.dao.member

import com.dr.model.member.DrMemberBank
import com.shangying.risk.core.mapper.IBaseMapper
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

@CompileStatic
@TypeChecked
interface DrMemberBankMapper extends IBaseMapper<DrMemberBank> {
}