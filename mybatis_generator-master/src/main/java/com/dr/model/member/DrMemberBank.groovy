package com.dr.model.member

import java.util.Date
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

@CompileStatic
@TypeChecked
class DrMemberBank implements Serializable {
    Integer id

    Integer uid

    String banknum

    String bankname

    String citycode

    String branchbankname

    String mobilephone

    Boolean type

    Boolean status

    Byte channel

    Date addtime

    Integer cardflag

    String agreementno

    Integer adduser

    static final long serialVersionUID = 1L
}