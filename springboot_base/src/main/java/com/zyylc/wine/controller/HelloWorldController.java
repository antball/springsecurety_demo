package com.zyylc.wine.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zyylc.wine.dto.WebConfig;
import com.zyylc.wine.entity.UserTest;
import com.zyylc.wine.enums.ResultTypeEnum;
import com.zyylc.wine.exception.BusinessException;
import com.zyylc.wine.util.FastJsonUtils;
import com.zyylc.wine.util.Result;

@Controller
public class HelloWorldController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(HelloWorldController.class);
	/*
	 * @Autowired UserService userService;
	 * 
	 * @Autowired StudentService studentService;
	 */
	@Autowired
	WebConfig webConfig;

	/**
	 * 返回json字符串
	 * 
	 * @return
	 */
	@RequestMapping("/helloWorld_JSON")
	@ResponseBody
	public String index() {
		UserTest tUsersPO = new UserTest();
		tUsersPO.setName("张学友");

		return FastJsonUtils.beanToJson(tUsersPO);
	}

	@RequestMapping("/users/{username}")
	public String userProfile(@PathVariable("username") String username) {
		return String.format("user %s", username);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginGet() {
		return "Login Page";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPost() {
		return "Login Post Request";
	}

	/**
	 * 跳转到jsp页面
	 * 
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @param request
	 * @param model
	 * @return
	 * @author: zj
	 */
	@RequestMapping(value = "/helloWorld")
	public String testjsp(HttpServletRequest request, Model model) {
		model.addAttribute("helloworld", "你好，世界");
		UserTest tUserPO = new UserTest();

		tUserPO.setName("刘德华2");
		// TUserPO po = userService.findUserOne(tUserPO);
		model.addAttribute("user", tUserPO);
		return "user/user";
	}

	/**
	 * 分页查询
	 * 
	 * @Title: findByPage
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @author zhangj
	 * @date 2018年7月15日 下午9:16:09
	 * @param pageNum
	 * @return
	 */
	@RequestMapping("/findByPage")
	@ResponseBody
	public String findByPage(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
			@RequestParam(value = "pageSize", required = false, defaultValue = "2") int pageSize) {
		PageHelper.startPage(pageNum, pageSize);// 设置分页参数
		// List<TUserPO> humorList = this.userService.findAllUser();
		PageInfo pageInfo = new PageInfo<>();

		return FastJsonUtils.beanToJson(pageInfo);
	}

	@RequestMapping("/findAllStudent")
	public String findAllStudent(Model model) {
		logger.info(webConfig.getAge() + webConfig.getName()
				+ "---------------------------------------------------------------------");
		throw new BusinessException(ResultTypeEnum.SYSTEM_ERROR.getCode(), ResultTypeEnum.SYSTEM_ERROR.getMsg());

		// model.addAttribute("student", studentService.findAllStudent());
		// return "/view/student";
	}

	@RequestMapping("/test1/{name}/{age}")
	@ResponseBody
	public String test1(Model model, @PathVariable String name, @PathVariable int age) {
		/*
		 * logger.info(webConfig.getAge() + webConfig.getName() +
		 * "---------------------------------------------------------------------");
		 */
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", name);
		map.put("age", String.valueOf(age));
		Result<Map> result = new Result<Map>();
		result.setData(map);
		return jsonResponse(result);
	}

	@RequestMapping(value = "/test3")
	@ResponseBody
	public Object test3(Model model, @RequestParam("myname") String name, @RequestParam("myage") int age) {
		logger.info(name);
		logger.info(age + "");
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", name);
		map.put("age", String.valueOf(age));
		Result<Map> result = new Result<Map>();
		result.setData(map);
		return jsonResponse(result);
	}

	@RequestMapping(value = "/test4")
	@ResponseBody
	public Object test4(Model model, String name,  String age) {
		logger.info(webConfig.getAge()+"=======================");
		logger.info(webConfig.getName()+"======================");
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", name);
		map.put("age", String.valueOf(age));
		return jsonResponse(map, ResultTypeEnum.SUCCESS.getCode(), ResultTypeEnum.SUCCESS.getMsg());
	}

	@RequestMapping(value = "/test5")
	@ResponseBody
	public Object test5(Model model, @RequestBody UserTest userTest) {
		Result result = new Result();
		result.setData(userTest);
		return result;
	}

}
