package com.zyylc.wine.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 对应的配置属性文件属性值映射到java类
 * 
 * @ClassName WebConfig
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 上午11:42:17
 * @version 1.0.0
 */
@Component
@ConfigurationProperties(prefix = "sysconfig")
@PropertySource("classpath:conf/sys-config.properties")
public class WebConfig {
	private String name;
	private String age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}



}
