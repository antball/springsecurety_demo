package com.zyylc.wine.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 参数不能为null 或者"", 用于 controller 参数校验
 * 
 * @ClassName ParamNotNull
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 下午2:19:56
 * @version 1.0.0
 */
@Target(ElementType.PARAMETER) // @Target(ElementType.PARAMETER)表示该注解作用于方法参数上
@Retention(RetentionPolicy.RUNTIME) // @Retention注解保留在程序运行期间，此时可以通过反射获得定义在某个类上的所有注解。
public @interface CheckParam {
	/**
	 * 是否非空,默认不能为空
	 */
	boolean notNull() default true;
}
