package com.zyylc.wine.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 本地缓存管理
 * 
 * @ClassName MapCacheManager
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zhangj
 * @Date 2017年5月19日 下午3:17:48
 * @version 1.0.0
 */
public class CacheManager {
	private final static Logger log = LoggerFactory.getLogger(CacheManager.class);
	private Map<String, Object> map = new ConcurrentHashMap<String, Object>();

	private static CacheManager instance = new CacheManager();

	private CacheManager() {
	}

	public synchronized static CacheManager getInstance() {
		if (instance == null) {
			instance = new CacheManager();
		}
		return instance;
	}

	public void put(String key, String value) {
		map.put(key, value);
	}

	public Object get(String key) {
		return map.get(key);
	}

	public boolean checkKey(String key) {
		if (map.containsKey(key)) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		CacheManager cacheManager = CacheManager.getInstance();
		for (int i = 0; i < 5; i++) {
			if (cacheManager.checkKey("1")) {
				System.out.println("有key");
				System.out.println(cacheManager.get("1"));
			} else {
				System.out.println("无key");
				cacheManager.put("1", "cccc");
			}
		}

	}
}
