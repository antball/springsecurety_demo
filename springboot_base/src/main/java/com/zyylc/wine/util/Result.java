package com.zyylc.wine.util;

import com.zyylc.wine.enums.ResultTypeEnum;

/**
 * 请求结果状态封装对象
 * 
 * @ClassName ResultVO
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 下午3:52:28
 * @version 1.0.0
 */
public class Result<T> {

	public Result(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public Result() {
		super();
		this.code = ResultTypeEnum.SUCCESS.getCode();
		this.msg = ResultTypeEnum.SUCCESS.getMsg();
	}

	/** 错误码. */
	private String code;

	/** 提示信息. */
	private String msg;
	private T data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
