package com.zyylc.wine.util;

import java.io.File;

public class FileUtil {

	/**
	 * 获取web 应用的绝对路径. 如： ../tomcat/webapps/{app}/
	 * @return
	 */
	public String getWebRootPath() {
		return new File(this.getClass().getClassLoader().getResource("").getPath()).getParentFile().getParentFile().getAbsolutePath();
	}
}
