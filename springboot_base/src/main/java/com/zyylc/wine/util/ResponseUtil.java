package com.zyylc.wine.util;

import com.alibaba.fastjson.JSONObject;

/**
 * response常用工具类
 * 
 * @ClassName ResponseUtil
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 下午3:35:37
 * @version 1.0.0
 */
public class ResponseUtil {
	private ResponseUtil() {
	}

	/**
	 * 对象转为json字符串
	 * 
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @param obj
	 * @return
	 * @author: zj
	 */
	public static String returnJsonData(Object obj) {
		if (obj != null) {
			return JSONObject.toJSONString(obj);
		} else {
			return "";
		}
	}
}
