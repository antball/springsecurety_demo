package com.zyylc.wine.util;

public class SceneInfo {
	// 场景类型
	private String type;
	// WAP网站URL地址
	private String wap_name;
	// WAP 网站名
	private String wap_url;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWap_name() {
		return wap_name;
	}

	public void setWap_name(String wapName) {
		wap_name = wapName;
	}

	public String getWap_url() {
		return wap_url;
	}

	public void setWap_url(String wapUrl) {
		wap_url = wapUrl;
	}
}
