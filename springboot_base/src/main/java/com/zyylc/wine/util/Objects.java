package com.zyylc.wine.util;

public class Objects {
	public static String toString(Object obj) {
		return obj == null ? "null" : obj.toString();
	}
	
	public static String toString(Object obj, String nullDefaultValue) {
		return obj == null ? nullDefaultValue : obj.toString();
	}
}
