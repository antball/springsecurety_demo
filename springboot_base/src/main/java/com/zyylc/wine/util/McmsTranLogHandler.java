package com.zyylc.wine.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class McmsTranLogHandler {
	
	private static ExecutorService executorService = null;

	private static int poolSize = 100;
	public static ExecutorService getExecutorService(){
		if(executorService != null)
			return executorService;
		synchronized(McmsTranLogHandler.class){
			executorService = Executors.newFixedThreadPool(poolSize);
			return executorService;
		}
	}
}
