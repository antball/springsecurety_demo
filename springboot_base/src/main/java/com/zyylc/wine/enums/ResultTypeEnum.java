package com.zyylc.wine.enums;

/**
 * 自定义异常返回结果
 * 
 * @ClassName ExceptionResultEnum
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 下午3:46:26
 * @version 1.0.0
 */
public enum ResultTypeEnum {
	SYSTEM_ERROR("-1", "系统错误"), 
	SUCCESS("0", "成功"), 
	ERROR("1", "失败");
	private String code;
	private String msg;

	ResultTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}
