package com.zyylc.wine.exception;

import com.zyylc.wine.enums.ResultTypeEnum;

/**
 * 自定义业务异常类
 * 
 * @ClassName: BusinessException
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangj
 * @date 2018年7月21日 上午9:25:27
 */
public class AppException extends RuntimeException {
	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;
	private String code; // 错误码

	public AppException() {
	}

	public AppException(ResultTypeEnum resultEnum) {
		super(resultEnum.getMsg());
		this.code = resultEnum.getCode();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
