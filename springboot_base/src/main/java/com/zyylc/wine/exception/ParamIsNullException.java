package com.zyylc.wine.exception;

/**
 * 参数校验 当参数为空的时候，抛出该异常
 * 
 * @ClassName ParamIsNullException
 * @Description TODO(这里用一句话描述这个类的作用)
 * @author zj
 * @Date 2018年7月26日 下午2:29:22
 * @version 1.0.0
 */
public class ParamIsNullException extends RuntimeException {
	/**
	 * @Field @serialVersionUID : TODO(这里用一句话描述这个类的作用)
	 */
	private static final long serialVersionUID = 1L;
	private final String parameterName;
	private final String parameterType;

	public ParamIsNullException(String parameterName, String parameterType) {
		super("");
		this.parameterName = parameterName;
		this.parameterType = parameterType;
	}

	@Override
	public String getMessage() {
		return "Required " + this.parameterType + " parameter \'" + this.parameterName + "\' must be not null !";
	}

	public final String getParameterName() {
		return this.parameterName;
	}

	public final String getParameterType() {
		return this.parameterType;
	}

}
