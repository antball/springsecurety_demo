package com.zyylc.wine.exception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zyylc.wine.enums.ResultTypeEnum;
import com.zyylc.wine.util.FastJsonUtils;
import com.zyylc.wine.util.ResponseUtil;
import com.zyylc.wine.util.Result;

/**
 * 全局异常处理
 * 
 * @ClassName: GlobalDefaultExceptionHandler
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangj
 * @date 2018年7月21日 上午9:10:06
 */
@ControllerAdvice
public class GlobalDefaultException {
	private static Logger logger = LoggerFactory.getLogger(GlobalDefaultException.class);

	/**
	 * 如果返回的是View -- 方法的返回值是ModelAndView;
	 * <P/>
	 * 如果返回的是String或者是Json数据，
	 * <P/>
	 * 那么需要在方法上添加@ResponseBody注解.
	 * 
	 * @Title: defaultExceptionHandler
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @author zhangj
	 * @date 2018年7月21日 上午9:13:44
	 * @param req
	 * @param e
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView defaultExceptionHandler(HttpServletRequest req, Exception e) {
		logger.error("错误的异常请求url===================\r\n" + req.getRequestURL());
		if (e instanceof BusinessException) {
			BusinessException businessException = (BusinessException) e;
			logger.error("业务逻辑异常==============\r\n" + businessException.getMsg());
		} else if (e instanceof DataDoException) {
			DataDoException dataDoException = (DataDoException) e;
			logger.error("数据库操作异常=============\r\n" + dataDoException.getMsg());
		} else {
			logger.error("系统异常============\r\n" + e.getMessage());
		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName("/error");
		return mav;

	}

	/**
	 * 参数为空异常处理
	 *
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ MissingServletRequestParameterException.class, ParamIsNullException.class })
	@ResponseBody
	public String requestMissingServletRequest(Exception e) {
		logger.error("参数为空异常=====", e);
		Result resultVO = new Result();

		if (e instanceof MissingServletRequestParameterException) {
			resultVO.setCode(ResultTypeEnum.SYSTEM_ERROR.getCode());
			resultVO.setMsg(ResultTypeEnum.SYSTEM_ERROR.getMsg());
		}
		return ResponseUtil.returnJsonData(resultVO);
	}

	@ExceptionHandler(value = AppException.class)
	@ResponseBody
	public String jsonExceptionHandler(HttpServletRequest req, Exception e) {
		Result resultVO = new Result();
		AppException appException = (AppException) e;
		resultVO.setCode(appException.getCode());
		resultVO.setMsg(appException.getMessage());
		logger.error("app端业务逻辑异常：\r\n" + e.getMessage());
		return FastJsonUtils.beanToJson(resultVO);
	}

	/**
	 * 判断是否是ajax请求
	 * 
	 * @Title: isAjax
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @author zhangj
	 * @date 2018年7月21日 下午11:02:03
	 * @param req
	 * @return
	 */
	public static Boolean isAjax(HttpServletRequest req) {
		return req.getHeader("X-Requested-With") != null
				&& "XMLHttpRequest".equals(req.getHeader("X-Requested-With").toString());
	}
}
