package com.zyylc.wine.exception;

/**
 * 自定义业务异常类
 * 
 * @ClassName: BusinessException
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangj
 * @date 2018年7月21日 上午9:25:27
 */
public class BusinessException extends RuntimeException {
	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String msg;

	public BusinessException(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
