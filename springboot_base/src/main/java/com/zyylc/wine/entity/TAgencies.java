package com.zyylc.wine.entity;

import java.util.Date;

public class TAgencies {
    private Integer id;

    private String name;

    private Date time;

    private Byte creditLevel;

    private Boolean isUse;

    private String idNumber;

    private String imagefilenames;

    private Integer bidCount;

    private Double bidAvgApr;

    private Integer successBidCount;

    private Integer overdueBidCount;

    private Integer badBidCount;

    private String introduction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Byte getCreditLevel() {
        return creditLevel;
    }

    public void setCreditLevel(Byte creditLevel) {
        this.creditLevel = creditLevel;
    }

    public Boolean getIsUse() {
        return isUse;
    }

    public void setIsUse(Boolean isUse) {
        this.isUse = isUse;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    public String getImagefilenames() {
        return imagefilenames;
    }

    public void setImagefilenames(String imagefilenames) {
        this.imagefilenames = imagefilenames == null ? null : imagefilenames.trim();
    }

    public Integer getBidCount() {
        return bidCount;
    }

    public void setBidCount(Integer bidCount) {
        this.bidCount = bidCount;
    }

    public Double getBidAvgApr() {
        return bidAvgApr;
    }

    public void setBidAvgApr(Double bidAvgApr) {
        this.bidAvgApr = bidAvgApr;
    }

    public Integer getSuccessBidCount() {
        return successBidCount;
    }

    public void setSuccessBidCount(Integer successBidCount) {
        this.successBidCount = successBidCount;
    }

    public Integer getOverdueBidCount() {
        return overdueBidCount;
    }

    public void setOverdueBidCount(Integer overdueBidCount) {
        this.overdueBidCount = overdueBidCount;
    }

    public Integer getBadBidCount() {
        return badBidCount;
    }

    public void setBadBidCount(Integer badBidCount) {
        this.badBidCount = badBidCount;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }
}