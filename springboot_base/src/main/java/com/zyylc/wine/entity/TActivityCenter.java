package com.zyylc.wine.entity;

public class TActivityCenter {
    private Long id;

    private String title;

    private String location;

    private String resolution;

    private String fileSize;

    private String fileFormat;

    private Integer order;

    private String url;

    private String firstImageUrl;

    private String infoImageUrl;

    private String ruleImageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution == null ? null : resolution.trim();
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize == null ? null : fileSize.trim();
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat == null ? null : fileFormat.trim();
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getFirstImageUrl() {
        return firstImageUrl;
    }

    public void setFirstImageUrl(String firstImageUrl) {
        this.firstImageUrl = firstImageUrl == null ? null : firstImageUrl.trim();
    }

    public String getInfoImageUrl() {
        return infoImageUrl;
    }

    public void setInfoImageUrl(String infoImageUrl) {
        this.infoImageUrl = infoImageUrl == null ? null : infoImageUrl.trim();
    }

    public String getRuleImageUrl() {
        return ruleImageUrl;
    }

    public void setRuleImageUrl(String ruleImageUrl) {
        this.ruleImageUrl = ruleImageUrl == null ? null : ruleImageUrl.trim();
    }
}