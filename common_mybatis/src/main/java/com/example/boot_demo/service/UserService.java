package com.example.boot_demo.service;


import com.example.boot_demo.dao.TUserMapper;
import com.example.boot_demo.model.TUser;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


//import com.example.boot_demo.dao.mapper.anocation.UserMapper;


@Component
public class UserService {

    @Autowired
//    @Qualifier("具体实现类名称")
    private TUserMapper userMapper;

    public TUser getUserById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }



    public List<TUser> findUsers(int pageNum, int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.selectAll();
    }

    public int add(TUser user) {
        return userMapper.insert(user);
    }

    public int update(TUser user) {
        return userMapper.updateByPrimaryKeySelective( user);
    }

    public int delete(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }
}
