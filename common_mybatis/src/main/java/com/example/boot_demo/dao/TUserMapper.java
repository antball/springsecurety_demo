package com.example.boot_demo.dao;

import com.example.boot_demo.model.TUser;
import tk.mybatis.mapper.common.Mapper;

public interface TUserMapper extends Mapper<TUser> {
}