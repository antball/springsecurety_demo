package com.example.boot_demo;

import com.example.boot_demo.model.TUser;
import com.example.boot_demo.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;


    @Test
    public void test() {
        Assert.assertEquals("1", userService.getUserById(1).getName());

        List<TUser> users = userService.findUsers(1,1);
        PageInfo pageInfo = new PageInfo(users);
        System.out.println("pages: "+pageInfo.getPages());
        Assert.assertEquals(7,pageInfo.getPages());

        Page  page =(Page<TUser>)users;
        System.out.println("totals : "+ page.getTotal());
        Assert.assertEquals(7,page.getTotal());


    }
}
