package com.juren.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juren.test.dao.TUserMapper;
import com.juren.test.model.TUser;
import com.juren.test.service.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	TUserMapper userDao;
	
	@Override
	public TUser getUserById(int uid) {
		return userDao.selectByPrimaryKey(uid);
	}
	
	

}
