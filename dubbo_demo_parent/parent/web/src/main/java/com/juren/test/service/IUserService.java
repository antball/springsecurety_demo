package com.juren.test.service;

import com.juren.test.model.TUser;

public interface IUserService {
	
	TUser getUserById(int uid);
}
