package cn.com.hellowood.springsecurity.listerner;

import cn.com.hellowood.springsecurity.service.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureListener
        implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    @Autowired
    private LoginAttemptService loginAttemptService;

    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e) {
//        WebAuthenticationDetails auth = (WebAuthenticationDetails)
//                e.getAuthentication().getDetails();
//        loginAttemptService.loginFailed(auth.getRemoteAddress());



//        YftUserDetails yftUserDetails = (YftUserDetails) authenticationSuccessEvent.getAuthentication().getPrincipal();
//        String account = yftUserDetails.getUsername();
//        Map<String, Object> user = userDao.queryUserByAccount(account);
//        userDao.updateStatusByAccount(account, user.get("ENABLE").toString(), 0);



//        e.getAuthentication().getPrincipal();

        loginAttemptService.loginFailed(e.getAuthentication().getName());


    }
}
