package cn.com.hellowood.springsecurity.service;

import cn.com.hellowood.springsecurity.mapper.UserMapper;
import cn.com.hellowood.springsecurity.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Service("userDetailsService")
public class CustomUserDetailsService  implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private HttpSession session;

    /**
     * 通过用户名和密码加载用户信息并校验
     *
     * @param username the username
     * @param password the password
     * @return the user model
     * @throws AuthenticationException the authentication exception
     */
    public UserModel loadUserByUsernameAndPassword(String username, String password) throws AuthenticationException {

        if (loginAttemptService.isBlocked(username)) {
            throw new LockedException("blocked");
        }


        logger.info("user {} is login by username and password", username);
        UserModel user = userMapper.getUserByUsernameAndPassword(username, password);
        validateUser(username, user);
        return user;
    }


    /**
     * 通过用户名加载用户信息，重写该方法用于记住密码后通过 Cookie 登录
     *
     * @param username
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("user {} is login by remember me cookie", username);
        UserModel user = userMapper.getUserByUsername(username);
        validateUser(username, user);
        return new User(user.getUsername(), user.getPassword(), new ArrayList<GrantedAuthority>());
    }

    /**
     * 校验用户信息并将用户信息放在 Session 中
     *
     * @param username
     * @param user
     */
    private void validateUser(String username, UserModel user) {
        if (user == null) {
            logger.error("user {} login failed, username or password is wrong", username);
            throw new BadCredentialsException("Username or password is not correct");
        } else if (!user.getEnabled()) {
            logger.error("user {} login failed, this account had expired", username);
            throw new AccountExpiredException("Account had expired");
        }
        // TODO There should add more logic to determine locked, expired and others status

        logger.info("user {} login success", username);
        // 当用户信息有效时放入 Session 中
        session.setAttribute("user", user);
    }
}
