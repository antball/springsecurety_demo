package com.mingzh.demo.service.impl;

import com.mingzh.demo.entity.User;
import com.mingzh.demo.mapper.UserMapper;
import com.mingzh.demo.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author huangsj
 * @since 2018-08-03
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
