package com.mingzh.demo.service;

import com.mingzh.demo.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author huangsj
 * @since 2018-08-03
 */
public interface IUserService extends IService<User> {

}
