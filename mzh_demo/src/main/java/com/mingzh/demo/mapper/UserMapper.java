package com.mingzh.demo.mapper;

import com.mingzh.demo.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author huangsj
 * @since 2018-08-03
 */
public interface UserMapper extends BaseMapper<User> {

}
