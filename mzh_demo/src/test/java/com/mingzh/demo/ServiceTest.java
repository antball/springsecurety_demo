package com.mingzh.demo;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mingzh.demo.entity.User;
import com.mingzh.demo.service.IUserService;
import javafx.application.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

    @Autowired
    IUserService iUserService;

    @Test
    public void selectTest(){
        List<User> list = iUserService.selectList(new EntityWrapper<User>());
        System.out.println("****************************************");
        for(User u:list){
            System.out.println(u.getType());
            Assert.assertNotNull("TypeEnum should not null",u.getType());
        }
        System.out.println("****************************************");
    }
}
