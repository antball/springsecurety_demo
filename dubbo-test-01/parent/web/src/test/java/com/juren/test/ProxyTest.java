package com.juren.test;

import com.juren.test.proxy.UserProxyFactory;
import com.juren.test.proxy.UserService;
import org.junit.Test;

/**
 * Created by DELL on 2017/12/6.
 */

public class ProxyTest {

    @Test
    public void testUserProxy() {

        UserService userService = UserProxyFactory.newInstance();

        userService.addUser();
    }

}
