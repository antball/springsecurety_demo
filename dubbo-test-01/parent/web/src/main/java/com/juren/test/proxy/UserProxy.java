package com.juren.test.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by DELL on 2017/12/6.
 */
public class UserProxy implements InvocationHandler,UserService {

    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("start .....");
//        method.invoke(target, args);
        method.invoke(this, args);
        System.out.println("end .....");
        return null;
    }


    public void addUser() {
        System.out.println("proxy add user");
    }
}
