package com.juren.test.proxy;

import java.lang.reflect.Proxy;

/**
 * Created by DELL on 2017/12/6.
 */
public class UserProxyFactory {

    public static UserService newInstance() {

        UserProxy userProxy = new UserProxy();
        UserService userService = new UserServiceImpl();
        userProxy.setTarget(userService);

       return (UserService) Proxy.newProxyInstance(
                        userService.getClass().getClassLoader(),
                        userService.getClass().getInterfaces(),
                        userProxy);

    }


}
