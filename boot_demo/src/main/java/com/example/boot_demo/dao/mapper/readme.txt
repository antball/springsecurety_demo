同一个表的注解和传统xml切换方法：

1. BootDemoApplication类中@MapperScan("com.example.boot_demo.dao.mapper.xml") 命名空间改成对应的
2. UserService类中UserMapper 引入命名空间改为对应的


也可以共用，但如果有注解方法，则xml中不应该有对应的方法，否则报错

