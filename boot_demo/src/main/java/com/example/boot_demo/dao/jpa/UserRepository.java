package com.example.boot_demo.dao.jpa;


import com.example.boot_demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  UserRepository  extends JpaRepository<User, Long> {

    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户名
     * @return 查询结果
     */
    List<User> findAllByName(String name);

    User findByName(String name);

    User findByNameAndAge(String name, Integer age);

    @Query("from t_user u where u.name=:name")
    User findUser(@Param("name") String name);
}
