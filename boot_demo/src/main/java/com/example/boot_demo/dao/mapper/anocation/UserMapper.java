package com.example.boot_demo.dao.mapper.anocation;

import com.example.boot_demo.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {

    @Select("SELECT * FROM t_user WHERE id = #{id}")
    User selectByPrimaryKey(Integer id);

    @Select("SELECT * FROM t_user")
    public List<User> getUserList();

    @Insert("insert into t_user(name, age) values(#{name}, #{age})")
    public int add(User user);

    @Update("UPDATE t_user SET name = #{user.name} , age = #{user.age} WHERE id = #{id}")
    public int update(@Param("id") Integer id, @Param("user") User user);

    @Delete("DELETE from t_user where id = #{id} ")
    public int delete(Integer id);
}
