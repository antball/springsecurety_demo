package com.example.boot_demo.service;


import com.example.boot_demo.dao.mapper.common.UserMapper;
import com.example.boot_demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


//import com.example.boot_demo.dao.mapper.anocation.UserMapper;

import java.util.List;

@Component
public class UserService {

    @Autowired
//    @Qualifier("具体实现类名称")
    private UserMapper userMapper;

//    public User getUserById(Integer id) {
//        return userMapper.selectByPrimaryKey(id);
//    }



    public int countByUsername(String username){
        return  userMapper.countByUsername(username);
    }



//    public List<User> getUserList() {
//        return userMapper.getUserList();
//    }
//
//    public int add(User user) {
//        return userMapper.add(user);
//    }
//
//    public int update(Integer id, User user) {
//        return userMapper.update(id, user);
//    }
//
//    public int delete(Integer id) {
//        return userMapper.delete(id);
//    }
}
