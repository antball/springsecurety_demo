package com.example.boot_demo;


import com.example.boot_demo.dao.jpa.UserRepository;
import com.example.boot_demo.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {


    @Autowired
    UserRepository userRepository;

    @Test
    public void test() {
//        User user = new User();
//        user.setName("钱三眼");
//        userRepository.save(user);

        Assert.assertEquals("1", userRepository.findAllByName("1").get(0).getName());

        Assert.assertEquals("1",userRepository.findByName("1").getName());

        Assert.assertEquals("1",userRepository.findByNameAndAge("1",1).getName());

        Assert.assertEquals("1",userRepository.findByName("1").getName());

        Pageable pageable = PageRequest.of(0, 2, Sort.by(Sort.Order.desc("id")));
        Page<User> users = userRepository.findAll(pageable);
        Assert.assertEquals(7, users.getTotalElements());



        User user = new User();
        user.setName("1");
        Example<User> userExample = Example.of(user);


        Example.of(user, ExampleMatcher.matching().withMatcher("name",
                /*startsWith -> 10010%
                 * endsWith -> %10010
                 * contains -> %10010%
                 * */
                ExampleMatcher.GenericPropertyMatchers.startsWith()));


        userRepository.findAll(userExample,pageable);


    }
}
