package com.example.boot_demo;


import com.example.boot_demo.dao.jdbctemplate.UserDao;
import com.example.boot_demo.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    UserDao userDao;



    @Test
    public void getUser() {
       User user = userDao.getUser(1L);

        Assert.assertEquals("1",user.getName());
    }

    @Test
    public void delUser() {
        userDao.delUser(1L);
    }



}
