package com.example.boot_demo;

import com.example.boot_demo.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;


    @Test
    public void test() {
//        Assert.assertEquals("1", userService.getUserById(1).getName());

        Assert.assertEquals(1, userService.countByUsername("1"));

    }
}
