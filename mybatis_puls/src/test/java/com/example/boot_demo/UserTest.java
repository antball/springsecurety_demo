package com.example.boot_demo;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.example.boot_demo.model.TUser;
import com.example.boot_demo.service.ITUserService;
import com.example.boot_demo.service.RedisService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {




    @Autowired
    ITUserService userService;

    @Test
    public void test(){

      Assert.assertEquals("1", userService.selectById(1).getName());

      Page<TUser> users =  userService.selectPage(new Page<TUser>(1,1),
                new EntityWrapper<TUser>().where("1=1"));

        Assert.assertEquals(7, users.getTotal());

        System.out.println(userService.selectUserPage(new Page<TUser>(1,1),10).getTotal());

    }


}
