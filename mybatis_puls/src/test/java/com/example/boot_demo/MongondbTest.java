package com.example.boot_demo;

import com.example.boot_demo.model.TUser;
import com.example.boot_demo.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MongondbTest {

    @Autowired
    private UserRepository userRepository;


    @Before
    public void setUp() {
        userRepository.deleteAll();
    }

    @Test
    public void test() throws Exception {
        // 创建三个User，并验证User总数
        TUser user =new TUser();
        user.setId(1);
        user.setName("1");
        user.setAddress("1");
        user.setAge(1);
        user.setPhone("1");
        user.setRemark("1");
        userRepository.save(user);

        user =new TUser();
        user.setId(3);
        user.setName("3");
        user.setAddress("3");
        user.setAge(3);
        user.setPhone("3");
        user.setRemark("3");
        userRepository.save(user);

        Assert.assertEquals(1, userRepository.findByName("1").getId().intValue());
        Assert.assertEquals(1, userRepository.getByName("1").getId().intValue());
    }


    /*
     1）查询所有的数据：
    public List<Person> queryAll() throws Exception {
        return personRepository.findAll();
    }

    2）查询所有的数据带分页：
    方法为：Page<?> findAll(Pageable pageable); 该方法中的参数是一个借口，我们需要构造一个子对象，即：PageRequest对象,这个对象中有两个属性，第一个是页码，第二个是页面大小。注意：页码在mongodb中是从0开始的。
    public Page<Person> queryAllByPage(int page,int rows) throws Exception {
        PageRequest pageRequest = new PageRequest(page-1,rows);
        return personRepository.findAll(pageRequest);
    }

    3）查询所有的数据的数量：
    public int count() throws Exception {
        long size = personRepository.count();
        int count = Integer.valueOf(String.valueOf(size));
        return count;
    }

    4）根据实体类中的属性进行查询：
    当需要根据实体类中的属性查询时，MongoRepository提供的方法已经不能满足，我们需要在PersonRepository仓库中定义方法，定义方法名的规则为：find + By + 属性名（首字母大写），如：根据姓名查询Person
    仓库中添加的方法：
    public Person findByName(String name);
    它会自动根据name查询。
    service中的方法：
    public void queryByFirstName(String name) throws Exception {
        Person person = personRepository.findByName(name);
    }
    若根据其他属性查询，方法类似！

            5）根据实体类中的属性进行模糊查询：
    当需要根据实体类中的属性进行模糊查询时，我们也需要在PersonRepository仓库中定义方法，模糊查询定义方法名的规则为：find + By + 属性名（首字母大写） + Like，如：根据姓名进行模糊查询Person
    仓库中添加的方法：
    public List<Person> findByNameLike(String name);
    service中的方法：
    在service中直接调用仓库中我们刚才定义的方法即可！
    public List<Person> queryByFirstNameLike(String name) throws Exception {
        return personRepository.findByNameLike(name);
    }

    6）根据实体类中的属性进行模糊查询带分页：
    带分页的模糊查询，其实是把模糊查询以及分页进行合并，同时我们也需要在PersonRepository仓库中定义方法，定义方法名的规则和模糊查询的规则一致，只是参数不同而已。
    仓库中添加的方法：
    public Page<Person> findByNameLike(String name,Pageable pageable);
    在service中对仓库中的方法的调用：
    public List<Person> queryByNameAndPage(int page, int rows, String name)
            throws Exception {
        PageRequest pageRequest = new PageRequest(page-1,rows);

        return personRepository.findByNameLike(name, pageRequest).getContent();
    }

    7）根据实体类中的属性进行模糊查询带分页，同时指定返回的键（数据库中的key,实体类中的属性）：
    解释一下什么是指定返回的键：也就是说当我们进行带分页的模糊查询时，不想返回数据库中的所有字段，只是返回一部分字段。例如：只返回Person中的id和name，不返回age.
            若想指定返回的键，我们需要在PersonRepository中添加方法，同时使用注解@Query。
    仓库中定义的方法：
    @Query(value="{'name':?0}",fields="{'name':1}")
    public Page<Person> findByNameLike(String name,Pageable pageable);
    其中value是查询的条件，？0这个是占位符，对应着方法中参数中的第一个参数，如果对应的是第二个参数则为？1。fields是我们指定的返回字段，其中id是自动返回的，不用我们指定，bson中{'name':1}的1代表true，也就是代表返回的意思。
    在service中对仓库中的方法的调用：
    public List<Person> queryByNameAndPage(int page, int rows, String name)
            throws Exception {
        PageRequest pageRequest = new PageRequest(page-1,rows);
        return personRepository.findByNameLike(name, pageRequest).getContent();
    }

    特殊查询：
            1）需要查询所有数据，同时指定返回的键
    当我们需要查询所有数据，同时需要指定返回的键时，则不能使用仓库中自带的findAll（）方法了。我们可以查询所有id不为空的数据，同时指定返回的键。当我们需要根据一个key且该key不为空进行查询，方法名的定义规则为：find + By + 属性名（首字母大写） + NotNull。
    仓库中定义的方法：
    @Query(value="{'_id':{'$ne':null}}",fields="{'name':1}")
    public Page<Person> findByIdNotNull(Pageable pageable);
    service中调用仓库中的方法：
    public List<Person> queryAll(int page, int rows) throws Exception {
        PageRequest pageRequest = new PageRequest(page-1,rows);
        return personRepository.findByIdNotNull(pageRequest).getContent();
    }
    2）MongoDB的其他查询不一一列举，但将java中的仓库定义的方法名的规则列举如下，使用时将仓库中方法上的注解@Query中的value进行适当泰欧正即可。
    GreaterThan(大于)
    方法名举例：findByAgeGreaterThan(int age)
    query中的value举例：{"age" : {"$gt" : age}}

    LessThan（小于）
    方法名举例：findByAgeLessThan(int age)
    query中的value举例：{"age" : {"$lt" : age}}

    Between（在...之间）
    方法名举例：findByAgeBetween(int from, int to)
    query中的value举例：{"age" : {"$gt" : from, "$lt" : to}}

    Not（不包含）
    方法名举例：findByNameNot(String name)
    query中的value举例：{"age" : {"$ne" : name}}

    Near（查询地理位置相近的）
    方法名举例：findByLocationNear(Point point)
    query中的value举例：{"location" : {"$near" : [x,y]}}
    */


}
