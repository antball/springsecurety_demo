package com.example.boot_demo.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.example.boot_demo.model.TUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huangsj
 * @since 2018-07-02
 */
public interface TUserMapper extends BaseMapper<TUser> {

    List<TUser> selectUserList(Pagination page, Integer age);

//    @Select("SELECT * FROM users WHERE id = #{id}")
//    @Results({
//            @Result(property = "userSex",  column = "user_sex", javaType = UserSexEnum.class),
//            @Result(property = "nickName", column = "nick_name")
//    })
//    UserEntity getOne(Long id);
//    http://www.mybatis.org/mybatis-3/zh/java-api.html

}
