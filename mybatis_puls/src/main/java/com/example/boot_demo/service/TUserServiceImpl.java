package com.example.boot_demo.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.example.boot_demo.model.TUser;
import com.example.boot_demo.dao.TUserMapper;
import com.example.boot_demo.service.ITUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huangsj
 * @since 2018-07-02
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {


    @Autowired
    TUserMapper userMapper;

    @Override
    public Page<TUser> selectUserPage(Page<TUser> page, Integer age) {


        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题
        // page.setOptimizeCountSql(false);
        // 不查询总记录数
        // page.setSearchCount(false);
        // 注意！！ 分页 total 是经过插件自动 回写 到传入 page 对象

        return page.setRecords(userMapper.selectUserList(page, age));
    }
}
