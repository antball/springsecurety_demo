package com.example.boot_demo.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.example.boot_demo.model.TUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huangsj
 * @since 2018-07-02
 */
public interface ITUserService extends IService<TUser> {

    public Page<TUser> selectUserPage(Page<TUser> page, Integer age);

}
