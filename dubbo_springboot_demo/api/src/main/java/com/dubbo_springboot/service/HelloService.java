package com.dubbo_springboot.service;

/**
 * @Auther: huangsj
 * @Date: 2018/8/28 11:46
 * @Description:
 */
public interface HelloService {

    String hello(String name);

}
