package com.dubbo_springboot.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dubbo_springboot.service.DemoService;
import com.dubbo_springboot.service.HelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: huangsj
 * @Date: 2018/8/27 10:45
 * @Description:
 */
@RestController
public class DemoConsumerController {

    @Reference(version = "${demo.service.version}"
//            application = "${dubbo.application.id}"
//            url = "dubbo://192.168.148.1:12345"  //配置url指向提供者，将绕过注册中心，多个地址用分号隔开.点对点
            )
    private DemoService demoService;


    @Reference(version = "${demo.service.version}")
    private HelloService helloService;

    @RequestMapping("/sayHello")
    public String sayHello(@RequestParam String name) {
        return demoService.sayHello(name);
    }


    @RequestMapping("/sayHello2")
    public String sayHello2(@RequestParam String name) {
        return demoService.sayHello2(name);
    }


    @RequestMapping("/sayHello3")
    public String sayHello3(@RequestParam String name) {
        return helloService.hello(name);
    }
}
