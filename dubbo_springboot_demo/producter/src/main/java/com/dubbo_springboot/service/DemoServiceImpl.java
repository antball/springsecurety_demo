package com.dubbo_springboot.service;

import com.alibaba.dubbo.config.annotation.Service;

import java.util.Date;

@Service(
        version = "${demo.service.version}",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}",
        register = false  //不注册到服务中心
)
public class DemoServiceImpl implements DemoService {

	@Override
    public String sayHello(String name) {
        return "Hello, " + name + ", " + new Date();
    }

    @Override
    public String sayHello2(String name) {
        return "Hello2, " + name + ", " + new Date();
    }

}