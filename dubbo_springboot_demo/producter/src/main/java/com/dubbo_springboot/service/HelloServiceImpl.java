package com.dubbo_springboot.service;

import com.alibaba.dubbo.config.annotation.Service;

import java.util.Date;

/**
 * @Auther: huangsj
 * @Date: 2018/8/28 11:58
 * @Description:
 */
@Service(
        version = "${demo.service.version}",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}"
)
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String name) {
        return "Hello Hello, " + name + ", " + new Date();
    }
}
