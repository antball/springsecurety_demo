package com.dubbo_springboot;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;


@SpringBootApplication
public class App 
{
    public static void main(String[] args) {

        new SpringApplicationBuilder(App.class)
                .web(false) // 非 Web 应用
                .run(args);

    }
}
