package com.dr.model.activity

import java.util.Date
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

@CompileStatic
@TypeChecked
class DrNewyearWish {
    Integer id

    Integer userId

    String phoneNum

    String wish

    Date creatTime

    Integer auditId

    Date auditTime

    Integer status
}