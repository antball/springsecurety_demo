package com.dr.dao.activity;

import com.dr.model.activity.TActivityIncreaseRate;

public interface TActivityIncreaseRateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TActivityIncreaseRate record);

    int insertSelective(TActivityIncreaseRate record);

    TActivityIncreaseRate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TActivityIncreaseRate record);

    int updateByPrimaryKey(TActivityIncreaseRate record);
}