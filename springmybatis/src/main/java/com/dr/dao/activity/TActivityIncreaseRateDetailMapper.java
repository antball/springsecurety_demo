package com.dr.dao.activity;

import com.dr.model.activity.TActivityIncreaseRateDetail;

public interface TActivityIncreaseRateDetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TActivityIncreaseRateDetail record);

    int insertSelective(TActivityIncreaseRateDetail record);

    TActivityIncreaseRateDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TActivityIncreaseRateDetail record);

    int updateByPrimaryKey(TActivityIncreaseRateDetail record);
}