package com.dr.dao.activity;

import com.dr.model.activity.DrNewyearWish;

public interface DrNewyearWishMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DrNewyearWish record);

    int insertSelective(DrNewyearWish record);

    DrNewyearWish selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DrNewyearWish record);

    int updateByPrimaryKey(DrNewyearWish record);
}