package com.test;


import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author yedm yedm@hundsun.com
 * @ClassName DateUtil
 * @Description 日期工具类
 * @date 2014-1-7
 */
public class DateUtil {
    /***** 增加统一的初始版本号 by luxf 20140616 *****/
    public static final String HUNDSUN_VERSION = "@system 三峡付 @version V2.0.1.1 @lastModiDate 20140616 @describe 最近修改记录";
    /* 秒 */
    public static final String DUE_UNIT_SECOND = "1";
    /* 分钟 */
    public static final String DUE_UNIT_MINUTE = "2";
    /* 小时 */
    public static final String DUE_UNIT_HOUR = "3";
    /* 天 */
    public static final String DUE_UNIT_DAY = "4";
    /* 月 */
    public static final String DUE_UNIT_MONTH = "5";
    /* 季度 */
    public static final String DUE_UNIT_QUARTER = "6";
    /* 年 */
    public static final String DUE_UNIT_YEAR = "7";

    /**
     * @param date
     * @param format
     * @return
     * @throws
     * @Title: dateToString
     * @Description: 将时间转换成字符串
     */
    public static String dateToString(Timestamp date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        // 将时间转换成字符串
        return dateFormat.format(date);
    }

    /**
     * @param date
     * @param format 转换得格式 例如"yyyy-MM-dd hh:mm:ss"
     * @return
     * @throws
     * @Title: dateToString
     * @Description: 将时间转换成按要求格式
     */
    public static String dateToString(java.util.Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        // 将时间转换成字符串
        return dateFormat.format(date);
    }

    // /**
    // * @Title: dateToDate
    // * @Description: 将时间转换成按要求格式
    // * @param date
    // * @param format 转换得格式 例如"yyyy-MM-dd hh:mm:ss"
    // * @return
    // * @throws
    // */
    // public static java.util.Date dateToDate(java.util.Date date, String
    // format) {
    // // 附加时间格式
    // dateFormat.applyPattern(format);
    // // 将时间转换成字符串
    // try {
    // return dateFormat.parse(dateToString(date, format));
    // } catch (ParseException e) {
    // LOGGER.error(e.getMessage(), e);
    // return null;
    // }
    // }

    /**
     * @param dateString 需要转换的时间字符串
     * @param format     转换得格式 例如"yyyy-MM-dd hh:mm:ss"
     * @return
     * @throws
     * @Title: stringToDate
     * @Description: 将字符串转换成时间
     */
    public static java.util.Date stringToDate(String dateString, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        // 将时间转换成字符串
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * @param date1
     * @param date2
     * @return
     * @throws
     * @Title: dateDiff
     * @Description: 比较两个时间的差值(以秒为单位)
     */
    public static long dateDiff(java.util.Date date1, java.util.Date date2) {
        return date2.getTime() / 1000 - date1.getTime() / 1000; // 用立即数，减少乘法计算的开销
    }

    /**
     * @return
     * @throws
     * @Title: date
     * @Description: 获取当前日期 格式 2012－10－22 返回类型：Date 参数：null
     */
    public static Date date() {
        // 获取当前日期
        String strDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        // 将字符串日期转换成 java.sql.Date 日期类型
        return Date.valueOf(strDate);
    }

    /**
     * @param day 天数 负数代表前几天，正数代表后几天
     * @return
     * @throws
     * @Title: getDateByDay
     * @Description: 获取当前日期的前几天或者后几天日期
     */
    public static Date getDateByDay(int day) {
        // 获取当前日期
        Calendar date = Calendar.getInstance();
        date.set(Calendar.DATE, date.get(Calendar.DATE) + day);
        String strDate = new SimpleDateFormat("yyyy-MM-dd").format(date.getTime());
        // 将字符串日期转换成 java.sql.Date 日期类型
        return Date.valueOf(strDate);
    }

    /**
     * @return
     * @throws
     * @Title: datetime
     * @Description: 获取当前日期 格式 2007－3－4 12：10：20 返回类型：Date 参数：null
     */
    public static Timestamp datetime() {
        // 获取当前日期
        String strTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        // 将字符串日期转换成 java.sql.Date 日期类型
        return Timestamp.valueOf(strTimestamp);
    }

    /**
     * @return
     * @throws
     * @Title: datetimeByString
     * @Description: 获取当前日期 格式 2007－3－4 12：10：20 返回类型：Date 参数：null
     */
    public static String datetimeByString() {
        // 获取当前日期
        String strTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        // 将字符串日期转换成 java.sql.Date 日期类型
        return strTimestamp;
    }

    /**
     * @return
     * @throws
     * @Title: time
     * @Description: 获取当前时间 格式 12：10：20 返回类型：Date 参数：null
     */
    public static Time timeNow() {
        // 获取当前日期
        String strTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
        // 将字符串日期转换成 java.sql.Date 日期类型
        return Time.valueOf(strTime);
    }

    /**
     * 取得当前的毫秒
     *
     * @param
     * @return 当前日期
     * @throws
     * @Title: getCurrentDateMill
     * @Description: 取得当前日期的String
     * @author wuhao08298
     */
    public static String getCurrentDatemill() {
        String tDate = new String();
        Calendar Cld = Calendar.getInstance();
        tDate = tDate + Cld.get(Calendar.MILLISECOND);
        return tDate;
    }

    /**
     * 取得当前日期
     *
     * @param aFormat 日期格式
     * @return 当前日期
     * @throws
     * @Title: getCurrentDate
     * @Description: 取得当前日期的String
     */
    public static String getCurrentDate(String aFormat) {
        TimeZone time = TimeZone.getTimeZone("GMT+8"); // 设置为东八区
        time = TimeZone.getDefault();// 这个是国际化所用的
        TimeZone.setDefault(time);// 设置时区
        Calendar calendar = Calendar.getInstance();// 获取实例
        DateFormat format1 = new SimpleDateFormat(aFormat);// 构造格式化模板
        java.util.Date date = (java.util.Date) calendar.getTime(); // 获取Date对象
        String tDate = format1.format(date);
        return tDate;
    }

    /**
     * 获取当前日期。 <br>
     * 获取的日期格式为yyyyMMddHHmmss
     *
     * @return String - 返回当前日期
     */
    public static String getCurrentDateTime() {
        // 获取当前日期
        // String strDate = new
        // SimpleDateFormat("yyyyMMddHHmmss").format(Calendar
        // .getInstance().getTime());
        return getCurrentDate("yyyyMMddHHmmss");
    }

    /**
     * 获取当前14位时间+日期
     */
    public static String getCurrentDateTime14() {
        return getCurrentDate("yyyyMMddHHmmss");
    }

    /**
     * 获取当前8位日期
     */
    public static String getCurrentDate8() {
        return getCurrentDate("yyyyMMdd");
    }

    /**
     * 获取当前6位日期
     */
    public static String getCurrentDate6() {
        return getCurrentDate("yyyyMM");
    }

    /**
     * 获取当前6位完整日期
     */
    public static String getCurrentAllDate6() {
        return getCurrentDate("yyMMdd");
    }

    /**
     * 获取当前4位日期
     */
    public static String getCurrentDate4() {
        return getCurrentDate("yyyy");
    }

    /**
     * 获取当前6位时间
     */
    public static String getCurrentTime6() {
        return getCurrentDate("HHmmss");
    }

    /**
     * 获取当前时间
     *
     * @return
     * @Method: getCurrentDate
     * @Description: TODO(这里用一句话描述这个方法的作用)
     */
    public static java.util.Date getCurrentDate() {
        TimeZone time = TimeZone.getTimeZone("GMT+8"); // 设置为东八区
        time = TimeZone.getDefault();// 这个是国际化所用的
        TimeZone.setDefault(time);// 设置时区
        Calendar calendar = Calendar.getInstance();// 获取实例
        java.util.Date date = (java.util.Date) calendar.getTime(); // 获取Date对象
        return date;
    }

    /**
     * 判断是否过期
     *
     * @param startDate 开始时间
     * @param dueUnit   过期单位
     * @param dueValue  过期值
     * @return
     * @Method: isOverDue
     * @Description: TODO(这里用一句话描述这个方法的作用)
     */
    public static boolean isOverDue(Date startDate, String dueUnit, String dueValue) {
        java.util.Date dueDate = getOverDue(startDate, dueUnit, dueValue);
        java.util.Date currDate = getCurrentDate();
        if (dueDate != null && dueDate.getTime() < currDate.getTime()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取过期时间
     *
     * @param startDate
     * @param dueUnit
     * @param dueValue
     * @return
     * @Method: getOverDue
     * @Description: TODO(这里用一句话描述这个方法的作用)
     */
    public static java.util.Date getOverDue(java.util.Date startDate, String dueUnit, String dueValue) {
        /* 有效期单位:1-秒;2-分;3-小时;4-天;5-月;6-季;7-年 */
        java.util.Date dueDate = null;
        if (DUE_UNIT_SECOND.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.SECOND, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_MINUTE.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.MINUTE, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_HOUR.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_DAY.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.DAY_OF_MONTH, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_MONTH.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.MONTH, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_QUARTER.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.MONTH, 3 * Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        } else if (DUE_UNIT_YEAR.equals(dueUnit)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.YEAR, Integer.parseInt(dueValue));
            dueDate = cal.getTime();
        }
        return dueDate;
    }

    /**
     * @param month
     * @return
     * @Method: getFirstDayofMonth
     * @Description: 获取一个月的第一天
     */
    public static String getFirstDayofMonth(String month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(month.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(month.substring(4, 6)));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, -1);
        return DateUtil.dateToString(cal.getTime(), "yyyyMMdd");
    }

    /**
     * @param month
     * @return
     * @Method: getLastDayofMonth
     * @Description: 获取一个月的最后一天
     */
    public static String getLastDayofMonth(String month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(month.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(month.substring(4, 6)));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return DateUtil.dateToString(cal.getTime(), "yyyyMMdd");
    }

    /**
     * @param dateString
     * @return
     * @Method: getLastMonth
     * @Description: 根据当前日期获取上一个月
     */
    public static String getLastMonth(String dateString) {
        String lastYearMonth = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.stringToDate(dateString, "yyyyMMdd"));
        cal.add(Calendar.MONTH, -1);
        lastYearMonth = DateUtil.dateToString(cal.getTime(), "yyyyMM");
        return lastYearMonth;
    }

    /**
     * @param dateString
     * @return
     * @Method: getLastYear
     * @Description: 根据当前日期获取上一年
     */
    public static String getLastYear(String dateString) {
        String lastYear = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.stringToDate(dateString, "yyyyMMdd"));
        cal.add(Calendar.YEAR, -1);
        lastYear = DateUtil.dateToString(cal.getTime(), "yyyy");
        return lastYear;
    }

    /**
     * 获取的日期格式为yyyyMMdd
     *
     * @param @return 返回当前日期
     * @return String
     * @Method: getDate
     * @Description: 获取的日期格式为yyyyMMdd
     */
    public static String getDate() {
        return getCurrentDate8();
    }

    /**
     * 获取当前时间，格式HHmmss
     *
     * @param @return 获取当前时间
     * @return String
     * @Method: getTime
     * @Description: 获取当前时间，格式HHmmss
     */
    public static String getTime() {
        return getCurrentTime6();
    }

    /**
     * 相差时间，格式yyyyMMddHHmmss
     *
     * @param @return 获取相差时间
     * @return String
     * @Method: getDiffentTime
     * @Description: 获取当前时间，格式yyyyMMddHHmmss
     */
    public static String getDiffentTime(String synInitTime, Integer intervalTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.stringToDate(synInitTime, "yyyyMMddHHmmss"));
        calendar.add(Calendar.SECOND, intervalTime);
        return dateToString(calendar.getTime(), "yyyyMMddHHmmss");
    }

    /**
     * 获取时间差的天数，格式HHmmss
     *
     * @param @return 获取当前时间
     * @return String
     * @throws ParseException
     * @Method: getTime
     * @Description: 获取当前时间，格式HHmmss
     */
    public static int getDifferentDate(String previousDate, String nextDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(previousDate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(nextDate));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * @param date
     * @param years
     * @return
     * @Method: getFewYearsLater
     * @Description: 获取当前时间后years的时间
     */
    public static String getFewYearsLater(String date, int years) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.stringToDate(date, "yyyyMMddHHmmss"));
        calendar.add(Calendar.YEAR, years);
        return dateToString(calendar.getTime(), "yyyyMMddHHmmss");
    }
}
