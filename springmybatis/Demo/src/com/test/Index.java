package com.test;

import java.math.BigDecimal;

public class Index {


    public static void main(String[] ags) throws Exception {

        String originalYieldStr = "9.5";
        String initDate = "20171226000000";
        String principal = "182500.00";
        System.out.println(DateUtil.getDifferentDate(initDate.substring(0, 8), DateUtil.getCurrentDate8()));

        BigDecimal repaymentAmt = BigDecimal.ZERO;
        BigDecimal yield = BigDecimal.ZERO;
        BigDecimal originalYield = new BigDecimal(originalYieldStr).divide(new BigDecimal(100));
        BigDecimal differentDate = new BigDecimal(DateUtil.getDifferentDate(initDate.substring(0, 8), DateUtil.getCurrentDate8()) + 1);
        BigDecimal advancePrincipal = new BigDecimal(principal);

        System.out.println(new BigDecimal(365).add(originalYield.multiply(differentDate)));


        repaymentAmt = advancePrincipal
                .multiply(new BigDecimal(365).add(originalYield.multiply(differentDate)))
        //.divide(new BigDecimal(365),2, BigDecimal.ROUND_UP)
        ;
        yield = repaymentAmt.subtract(advancePrincipal);


        System.out.println("应还金额：" + repaymentAmt.toString());
        System.out.println("应还利息：" + yield.toString());

    }
}
