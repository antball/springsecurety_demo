package com.forezp.servicelucy.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

/**
 * @Auther: huangsj
 * @Date: 2019/5/24 14:00
 * @Description:
 */
@RestController
public class HelloController {

    private static final Logger logger = Logger.getLogger(HelloController.class.getName());

    @Value("${server.port}")
    String port;

    @RequestMapping("/hello")
    @HystrixCommand(fallbackMethod = "helloError")
    public String home(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        logger.info("2222222222");
        return "hi " + name + " ,i am from port:" + port;
    }

    public String helloError(String name) {
        return "hi,"+name+",sorry,error!";
    }


}
