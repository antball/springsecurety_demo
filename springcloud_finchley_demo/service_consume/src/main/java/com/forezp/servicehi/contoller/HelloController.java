package com.forezp.servicehi.contoller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Auther: huangsj
 * @Date: 2019/5/24 13:58
 * @Description:
 */
@RestController
public class HelloController {

    @Autowired
    HelloRemote helloRemote;

    @Value("${server.port}")
    String port;

//    private final static Logger logger = LoggerFactory.getLogger(HelloController.class);
    private  final Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping(value = "/hi")
    public String home(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        logger.info("111111111");
        return helloRemote.sayHiFromClientOne(name);
    }

}
