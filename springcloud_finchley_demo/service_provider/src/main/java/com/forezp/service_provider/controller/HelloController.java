package com.forezp.service_provider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

/**
 * @Auther: huangsj
 * @Date: 2019/5/24 14:18
 * @Description:
 */

@RestController
public class HelloController {

    private static final Logger logger = Logger.getLogger(HelloController.class.getName());

    @RequestMapping("/hi")
    public String home(@RequestParam(value = "name", defaultValue = "china") String name) {
        logger.info("fsfsafsf");
        return "hi " + name + " ,i am from service provider!!!!!!!!!!!";
    }
}
